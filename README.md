# cypress-example
This project was created for the fast demo during the interview at Smartmail.com.
It is a test automation framework draft for https://www.startmail.com. 

## Tech stack
- JavaScript
- Cypress 10
- ESlint 6.8.0

## Execution and contribution
You can find test specs in cypress/e2e. To run them, use `npm test`. 
You can also use some customised commands to run the tests, like:
- `npm run test:chrome` to run tests in Google Chrome. This browser is also set up as a default option.
- `npm run test:ff` to run the tests in pre-installed firefox. 
- `npm run test:edge` to use MS edge for the test run.
Please note that you will need to install the browser in CI or use docker image for that.
This framework uses page objects model (POM), so some useful methods for interaction with elements can be found in cypress/e2e/page_objects 

## Content
For the demo purposes there are two specs:
- `main.spec.cy.js` shows the approach of POM in e2e registration test. It does not include payment checks.
- `viewport.spec.cy.js` presents a draft for cross-device (cross-platform) testing

## Installation
To install dependencies, clone this project and run `npm install`

## Usage
To open Cypress console, use `npm run open`.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
### Testing
First step is to implement full functional testing of all user forms.
Second step is to implement cross-device and cross-browser testing using screenshot comparison.
### TestOps
1. Apply a pipeline configuration for this project
2. Set up parallelization
3. Increase browser- and device coverage 

## Authors and acknowledgment
In case of any questions or suggestions, please contact Anna Khvorostianova (testprogmath@gmail.com)

