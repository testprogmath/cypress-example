/* eslint-disable no-unused-vars */
const { defineConfig } = require("cypress");

module.exports = defineConfig({
  chromeWebSecurity: false,
  includeShadowDom: true,
  experimentalSourceRewriting: true, 
  setupNodeEvents(on, config) {
    on('before:browser:launch', (browser, launchOptions) => {
      if (browser.name === 'chrome') { 
      launchOptions.args.push('--proxy-bypass-list=<-loopback>')
      return launchOptions;
      }
    })
  },

  e2e: {
    baseUrl: "https://www.startmail.com/",
  },
  env: {
    userName: "Test Test",
    emailUserName: "mytestisthebest",
    defaultPassword: "paTssWord123",
    weakPassword: "password",
  }
});
