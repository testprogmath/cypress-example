module.exports = {
    "env": {
        "browser": true,
        "node": true,
    },
    "extends": ["eslint:recommended", "plugin:cypress/recommended"],
    "parserOptions": {
        "ecmaVersion": 8,
        "sourceType": "module"
    },
    "rules": {
        "cypress/no-assigning-return-values": "error",
        "cypress/no-unnecessary-waiting": "error",
        "cypress/assertion-before-screenshot": "warn",
        "cypress/no-force": "warn",
        "cypress/no-async-tests": "error",
        "cypress/no-pause": "error"
    },
    "plugins": [
        "cypress"
      ],
}
