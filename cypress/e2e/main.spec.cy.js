import 'cypress-iframe'
import MainPage from './page_objects/main.page';

const defaultUserName = Cypress.env('userName');
const defaultPassword = Cypress.env('defaultPassword');
const emailUserName = Cypress.env('emailUserName');
const weakPassword = Cypress.env('weakPassword');

describe('High priority tests', () => {
  it('main page is redirected to www.startmail.com/en/', () => {
    new MainPage().goTo();
    cy.url().should('contain', 'www.startmail.com/en/');
  });

  it('user can sign up for the account', () => {
    new MainPage().goTo()
      .clickLogInButton()
      .clickSignUpButton()
      .fillInPreferableName(defaultUserName)
      .fillInEmailUserName(emailUserName)
      .fillInPassword(defaultPassword)
      .fillInPasswordConfirmation(defaultPassword)
      .submitForm()
      .checkPrepaymentReferencePage()
      .clickNext()
      .checkPage();
  });

  it('user cannot use a weak password', () => {
    new MainPage().goTo()
      .clickLogInButton()
      .clickSignUpButton()
      .fillInPreferableName(defaultUserName)
      .fillInEmailUserName(emailUserName)
      .fillInPassword(weakPassword)
      .checkPasswordStrengthMessage('Weak password')
      .checkPasswordNotificationMessage();
  });

})