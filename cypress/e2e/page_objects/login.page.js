import SignUpPage from "./signup.page";

class LoginPage {
    navigateFromTheMainPage() {
        cy.get('.app-header__actions > .button-secondary').click();
    }

    clickSignUpButton() {
        cy.get('.button--accent').click();
        return new SignUpPage();
    }

}

export default LoginPage;