import LoginPage from "./login.page";

class MainPage {

    constructor() {
    }

    goTo() {
        cy.visit('/en');
        return this;
    }
    clickLogInButton() {
        cy.get('.app-header__actions > .button-secondary').click();
        return new LoginPage();
    }
}
export default MainPage;