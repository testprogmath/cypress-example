class PaymentDetailPage {
checkPage() {
    cy.intercept('**/api/v2/subscriptions/cart_sub_create**').as('createSubscription');
    cy.wait('@createSubscription').its('response.statusCode').should('eq', 200);
    cy.get('#cb-container').should('be.visible');
}
}
export default PaymentDetailPage;