
import PrePaymentReferencePage from "./prePaymentReference.page";

class SignUpPage {
    constructor() {

    }
    fillInPreferableName(name) {
        cy.get('#name').type(name);
        return this;
    }

    fillInEmailUserName(name) {
        cy.get('#mail-address').type(name);
        return this;
    }

    fillInPassword(password) {
        cy.get('#password').type(password);
        cy.intercept('/api/PasswordStrength').as('apiStrengthCheck');
        cy.wait('@apiStrengthCheck');
        return this;
    }

    //better to use enums for message types in ts
    checkPasswordStrengthMessage(message) {
        cy.get('.password-field__hint strong').should('have.text', message);
        return this;
    }

    checkPasswordNotificationMessage() {
        cy.get('.notification-block > p').should('have.text', 'StartMail accounts require Good or Strong passwords');
        return this;
    }

    fillInPasswordConfirmation(password) {
        cy.get('#password-repeat').type(password);
        return this;
    }
    
    submitForm() {
        cy.get('.loading-button').click();
        return new PrePaymentReferencePage();
    }
}
export default SignUpPage;