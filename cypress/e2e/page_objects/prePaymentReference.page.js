import PaymentDetailPage from "./paymentDetail.page";

class PrePaymentReferencePage {

    checkPrepaymentReferencePage() {
        cy.get('.information__header > span').should('contain', 'Why do I need to provide payment details upon sign up?');
        return this;
    }

    clickNext() {
        cy.get('.button--primary').click();
        return new PaymentDetailPage();
    }
}

export default PrePaymentReferencePage;