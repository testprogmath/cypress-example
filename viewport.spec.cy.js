/* eslint-disable cypress/no-unnecessary-waiting */
describe('Viewport', () => {
    before(() => {
        cy.visit('/')
    });
    ['macbook-15',
        'macbook-13',
        'macbook-11',
        'ipad-2',
        'ipad-mini',
        'iphone-x',
        'iphone-8',
        'iphone-se2',
        'iphone-6+',
        'iphone-6',
        'iphone-4',
        'samsung-s10',
        'samsung-note9'
    ].forEach((dimension) => {
        it(`Check that button is visible ${dimension}`, () => {
            cy.viewport(dimension, 'portrait');
            cy.get('a.hero-main__button').should('be.visible');
            cy.wait(200);
            cy.viewport(dimension, 'landscape');
            cy.get('a.hero-main__button').should('be.visible');
            cy.wait(200);
        })
    })
});